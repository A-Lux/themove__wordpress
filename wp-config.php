<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'template' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'T3AUvHK<vas:4I80d_>!1sVON}V#reZa<$}6AmL1FC@4oWF1N^=aB%%0he0[CrGp' );
define( 'SECURE_AUTH_KEY',  'R0##KCeZL@--Axw:$=*KywAkT`_ITpC~/^SVdh:2s(D?On{;jCy$^i6b&z,nhVtt' );
define( 'LOGGED_IN_KEY',    'p8~vAFrl}$7<S2na9/xVAmw]+[nPEC:FFvHls/{:[/,p+X5kCvozVbw#wj|?/z!-' );
define( 'NONCE_KEY',        'Pfwsz6ig4@5/|G<mr J&fdc>.hFLnk>=.W){I9*}%PLdkjR$aE3MoaAozXflXMK7' );
define( 'AUTH_SALT',        'vm)2){+{[O88IbJAUBFJ5|Dn1gVO04(ng,;.w#2vD@RBzaeMckj8m~(0`~Lme1F9' );
define( 'SECURE_AUTH_SALT', '+wl%/9a&9{e[gLFYS;Z>29lHYz3[LYnVv?3`I<C&3E_d;.HPDVg|XSJoFR&h._hC' );
define( 'LOGGED_IN_SALT',   'P ++cAp[9)x.q-CUEs`[z2R-gXZhPFYS/1{3mq#&iUnZ}2Yb8rBCWG^F$= n6g}Q' );
define( 'NONCE_SALT',       '3k0#{0VbJm*AjtNaEA>C]`QSz&)W4-qF]6yNvbjDwOA~|I :E{q>kS:yIc):=o1Y' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
